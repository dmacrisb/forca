# forca

#### REGRAS #######

- É possível inserir uma palavra ou escolher uma das predefinidas
- Só são consideradas letras minúsculas
- Palavras inseridas pelo usuário com letras maiúsculas são convertidas para minúsculas
- Letras maiúsculas tentadas pelo usuário são convertidas para minúsculas
- O jogador tem um limite de 6 erros
- Um jogador pode errar mais de uma vez a mesma letra (caso tenha esquecido que já tentou)
- O jogo acaba se a palavra for descoberta ou se o jogar errar 6 vezes