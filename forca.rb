class Game
    # Permite ao usuário digitar um palavra ou selecionar uma predefinida
    def wordChoice
        print "Digite 0 para digitar uma palavra ou um numero de 1 a 20 para utilizar uma palavra predefinida: "
        @word = gets.chomp
        @word = @word.to_i
        if @word == 0
            print "Digite a palavra: "
            @word = gets.chomp
            @word = @word.downcase
        elsif @word > 0 && @word <= 20
            @wordlist = File.read("words.txt").split
            i = 0
            while i < 20
                if @word == i+1 
                    @word = @wordlist[i]
                    break
                end
                i += 1
            end
        else
            puts "Escolha uma opcao valida"
            wordChoice
        end
    end
    # Chama a escolha de palavra e inicializa o jogo
    def start
        wordChoice
        gallows = Gallows.new(@word)
        loop do 
            verify = gallows.play
            if verify == true
                break
            end 
        end
        gallows.printGameOver
    end    
end

# Instancia o jogo
class Gallows
    def initialize(word)
        @word = word
        @letters = @word.split("")
        @maxErrors = 6
        @wronglist = []
        @rightlist = []
        @errors = 0
        @hits = 0
        @gameOver = false
    end
    # Imprime a mesa da forca
    def printtable
        2.times { puts "\n"}
        i = 0
        while i < @letters.length
            if @rightlist.include?(@letters[i])
                print " ", @letters[i]
            else     
                print " _"
            end
            i += 1
        end
    end
    # Executa a jogada e guarda nos respectivos arrays
    def play
        printHangman
        printtable
        print "\n\n\nDigite uma letra: "
        shot = gets.chomp
        shot = shot.downcase
        if @letters.include?(shot)
            if @rightlist.include?(shot)
                puts "\nA letra #{shot} já foi tentada"
            else
                for match in @letters
                    if match == shot
                        @hits += 1
                    end
                end
                puts "\nA palavra contém a letra #{shot}!\n\n"
                @rightlist.push(shot)
            end
        else
            puts "\nA palavra não contém a letra #{shot} :(\n\n"
            puts "\nErros restantes: #{@maxErrors-@errors-1}"
            @wronglist.push(shot)
            @errors += 1 
        end
        # Verifica o fim de jogo
        if @errors == @maxErrors || @hits == @letters.length
            @gameOver = true
            return @gameOver
        end
    end
    # Verifica a vitória ou derrota e imprime o fim de jogo
    def printGameOver
        printHangman
        printtable
        if @errors == @maxErrors
            print "\n\n\n\nVocê perdeu!\n\n\n\n"
        end
        if @hits == @letters.length
            print "\n\n\n\nParabéns, você venceu!!!\n\n\n\n"
        end
    end
    # Imprime o boneco da forca
    def printHangman
        puts "\n------------|"
        if @errors >= 1
            puts "            O"
        end
        if @errors == 2
            puts "           /" 
        end
        if @errors == 3
            puts "           /|" 
        end
        if @errors >= 4
            puts "           /|\\" 
        end
        if @errors == 5
            puts "           /" 
        end
        if @errors == 6
            puts "           / \\" 
        end
    end
end

newgame = Game.new
newgame.start
